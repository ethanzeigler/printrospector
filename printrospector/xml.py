from typing import Any, Dict
try:
    import lxml.etree as et

    _has_lxml = True
except ImportError:
    import xml.etree.ElementTree as et

    _has_lxml = False

from .object import DynamicObject


class XmlSerializer:
    def __init__(self):
        self._tree = et.Element("Objects")

    def serialize(self, obj: DynamicObject):
        self._serialize_impl(obj)

    def _serialize_impl(self, obj: DynamicObject, *, root: et.Element = None):
        # Allocate a new subelement that represents this object.
        obj_element = et.SubElement(
            root if root is not None else self._tree, "Class", Name=obj.name
        )

        for name, property in obj:
            # If the property is not a list already, convert it into one.
            if not isinstance(property, list):
                property = [property]

            for element in property:
                # Allocate a new node for the current list value.
                property_element = et.SubElement(obj_element, name)

                # Serialize either as a nested object or as another value.
                if isinstance(element, DynamicObject):
                    self._serialize_impl(element, root=property_element)
                else:
                    property_element.text = str(element)

    def write_to_output(self, out: str, pretty_print: bool):
        result = et.ElementTree(self._tree)
        if _has_lxml:
            result.write(out, encoding="utf-8", pretty_print=pretty_print)

        else:
            with open(out, "w", encoding="utf-8") as f:
                if pretty_print:
                    from xml.dom import minidom
                    from xml.etree.ElementTree import tostring

                    f.write(
                        minidom.parseString(tostring(result.getroot())).toprettyxml(
                            indent=" " * 4
                        )
                    )
                else:
                    result.write(f, encoding="utf-8")
