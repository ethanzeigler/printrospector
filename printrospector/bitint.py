from .bit_buffer import BitBuffer


def sign_extend(value, bits):
    sign_bit = 1 << (bits - 1)
    return (value & (sign_bit - 1)) - (value & sign_bit)


def read_bitint(buf: BitBuffer, nbits: int, *, signed: bool) -> int:
    if nbits % 8 == 0:
        # Optimization for full bytes.
        return int.from_bytes(
            buf.read_bytes(nbits >> 3), "little", signed=signed
        )

    value = sum(b << i for i, b in enumerate(buf.read_bits(nbits, False)))
    return sign_extend(value, nbits) if signed else value
